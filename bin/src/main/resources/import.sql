Insert into regiones (id, nombre) values (1, 'Sudamérica');
Insert into regiones (id, nombre) values (2, 'CentroAmérica');
Insert into regiones (id, nombre) values (3, 'Norteamérica');
Insert into regiones (id, nombre) values (4, 'Europa');
Insert into regiones (id, nombre) values (5, 'Asia');
Insert into regiones (id, nombre) values (6, 'Africa');
Insert into regiones (id, nombre) values (7, 'Oceanía');
Insert into regiones (id, nombre) values (8, 'Antártida');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (1, 'Andrés', 'Guzmán', 'ag@angularspringboot.com', '2018-01-08');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1, 'Luis', 'Guzmán', 'lg@angularspringboot.com', '2018-01-04');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (2, 'Carlos', 'Guzmán', 'cg@angularspringboot.com', '2018-01-11');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (3, 'Tesorera', 'Guzmán', 'tg@angularspringboot.com', '2018-08-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3, 'Manuel', 'Carlos', 'mc@angularspringboot.com', '2018-01-05');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (2, 'Carlos', 'Sopoa', 'cs@angularspringboot.com', '2018-11-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (5, 'Chema', 'Cagigas', 'cc@angularspringboot.com', '2018-12-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (5, 'Lulu', 'Shopper', 'ls@angularspringboot.com', '2018-03-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (6, 'Andrea', 'Terror', 'at@angularspringboot.com', '2017-06-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (7, 'Andrés', 'Guzmán', 'ag2@angularspringboot.com', '2018-01-08');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (2, 'Luis', 'Guzmán', 'lg2@angularspringboot.com', '2018-01-04');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1, 'Carlos', 'Guzmán', 'cg2@angularspringboot.com', '2018-01-11');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Tesorera', 'Guzmán', 'tg2@angularspringboot.com', '2018-08-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (5,'Manuel', 'Carlos', 'mc2@angularspringboot.com', '2018-01-05');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (5,'Carlos', 'Sopoa', 'cs2angularspringboot.com', '2018-11-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Chema', 'Cagigas', 'cc2@angularspringboot.com', '2018-12-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (7,'Lulu', 'Shopper', 'ls2@angularspringboot.com', '2018-03-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Andrea', 'Terror', 'at2@angularspringboot.com', '2017-06-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Andrés', 'Guzmán', 'ag3@angularspringboot.com', '2018-01-08');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Luis', 'Guzmán', 'lg3@angularspringboot.com', '2018-01-04');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (5,'Carlos', 'Guzmán', 'cg3@angularspringboot.com', '2018-01-11');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Tesorera', 'Guzmán', 'tg3@angularspringboot.com', '2018-08-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Manuel', 'Carlos', 'mc3@angularspringboot.com', '2018-01-05');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (7,'Carlos', 'Sopoa', 'cs3angularspringboot.com', '2018-11-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Chema', 'Cagigas', 'cc3@angularspringboot.com', '2018-12-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Lulu', 'Shopper', 'ls3@angularspringboot.com', '2018-03-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (8, 'Andrea', 'Terror', 'at3@angularspringboot.com', '2017-06-01');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (7,'Chema', 'Cagigas', 'cc5@angularspringboot.com', '2018-12-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (6,'Lulu', 'Shopper', 'ls5@angularspringboot.com', '2018-03-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (6,'Andrea', 'Terror', 'at5@angularspringboot.com', '2017-06-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (6,'Andrés', 'Guzmán', 'ag5@angularspringboot.com', '2018-01-08');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Luis', 'Guzmán', 'lg5@angularspringboot.com', '2018-01-04');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Carlos', 'Guzmán', 'cg5@angularspringboot.com', '2018-01-11');

Insert into clientes (region_id, nombre, apellido, email, create_At) values (1,'Tesorera', 'Guzmán', 'tg5@angularspringboot.com', '2018-08-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (6,'Manuel', 'Carlos', 'mc5@angularspringboot.com', '2018-01-05');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Carlos', 'Sopoa', 'cs5angularspringboot.com', '2018-11-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Chema', 'Cagigas', 'cc6@angularspringboot.com', '2018-12-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Lulu', 'Shopper', 'ls6@angularspringboot.com', '2018-03-01');
Insert into clientes (region_id, nombre, apellido, email, create_At) values (3,'Andrea', 'Terror', 'at6@angularspringboot.com', '2017-06-01');

/* Creamos algunos usuarios con roles */

Insert into `usuarios` (username, password, enabled, nombre, apellido, email) values ('andres', '$2a$10$2qyzNcDN6nuEyIBeg118fOlrhKAkgeLzhnGY3XVAW0ByFF4PEuzGG',1, 'Andrés', 'Guzmán', 'profesor@angularspring.com');
Insert into `usuarios` (username, password, enabled, nombre, apellido, email) values ('admin', '$2a$10$6fkGaES1c2EIKlNESHJqf.XmwJe7Ey8ibuVoRXNX8s0yss5xyrrX.',1, 'Carlos', 'Sopoa', 'cs5@angularspringboot.com');
Insert into `usuarios` (username, password, enabled, nombre, apellido, email) values ('chema', '$2a$10$k67QAvgRqsjPBkMdlIs8wuNXrzkNHahj5mUDXhJb.dix5A5SBTxue',1, 'Chema', 'Cagigas', 'cc6@angularspringboot.com');
Insert into `usuarios` (username, password, enabled, nombre, apellido, email) values ('ana', '$2a$10$BfEGD82m08Ew0MejVxUMSOTMRGxTNE/FYLciosVXKTFE6sD35tbGK',0, 'Ana', 'Ventanilla', 'av@angularspring.com');

Insert into `roles` (nombre) values ('ROLE_USER');
Insert into `roles` (nombre) values ('ROLE_ADMIN');

Insert into `usuarios_roles` (usuario_id, role_id) values (1,1);
Insert into `usuarios_roles` (usuario_id, role_id) values (2,1);
Insert into `usuarios_roles` (usuario_id, role_id) values (2,2);
Insert into `usuarios_roles` (usuario_id, role_id) values (3,2);





