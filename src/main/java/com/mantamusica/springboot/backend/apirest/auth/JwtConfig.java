package com.mantamusica.springboot.backend.apirest.auth;

public class JwtConfig {
    public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n"+
            "MIIEpAIBAAKCAQEAumjXsEB4c7zIZFcM+A5Ze7fv/+gvLx99EIKei5ueYecpQMli\n"+
            "GIn0ae1S/LhX+hRHwCbpM2HrXlEVx52R1Hs/Fcc2HPiWDw5KFiGKbVa8PXAYRBZR\n"+
            "WiyT/tJq3uEgHmCJ0JjuIUtB8yl24slxn9rtOPw3Jp56m9VEQJp7tYC8gs6CvTzA\n"+
            "giZTgVP0IgMuYVq5cIGAzAPBOeZZIT6rnNgKKfZKZFGO82ToR1goVEI/bXtXIKR9\n"+
            "WSMudURe1ecELy8LRHzR1BiogSkqOS2wYGM+yTXrMOLIFRI3PN3WX/EzeTJnQ3Yb\n"+
            "J7wve/19wOgJ4qhXZL9mNwpi0R/gFf7UERJ3EQIDAQABAoIBAQCC6p0IqM6vohcu\n"+
            "+UAfepx32j/Q/3Ub6EaCcOn+bIW4houfd4/QYhroxgdxe9o1xUZ6+hjvwEZ4UAQ2\n"+
            "1EAV9ZThq64ifKeO3V9E2buJfM/pai9wqifqZ8JsxcTO0VcoXm6LtMwRT/R6XVWk\n"+
            "gwus1CmkyuXE1qqPhSFRKe3WFBrQ1ht7BZC2Y78NwF/x5HqUL+K06AjkpAsIlB68\n"+
            "fXSqSs5ab1RkXPkqGoPnCVshoWgkbR4bBg4rqeytYM/ZiApnv5WnbDQSuj8bAFSp\n"+
            "2W+J88FSdSUfZ/bP8m9xdIGUmr6W0raQ74UUdoqIwq18fIqZuIPpPi0jWc8rtIhJ\n"+
            "KJK3oWQ9AoGBANxaUVN9rGycuKXwMekRcHFn6eOC7/1CLKFKQUHamYG6l+ODLOCQ\n"+
            "PsiwP4o3M+VRDWIQLdz3l4ZQa5gjNc461l6ZTEjHMQdJLXsYWK6PWINzBaTd67z4\n"+
            "9gJHYIcmP90a+kNCs6GktG57f9iOs1TsXFAQH4QUfeNm/OD9ucC2QuLzAoGBANiQ\n"+
            "zMfth9ObYCfgd0rb+q8l08pPr2bhHu7LR+okIzhdI3DJeaMYmpXIweUM/A3ve2AU\n"+
            "wDF/39RarZ6k/PndQ6uZzFQVldHmrRQxiLeZ7UkIJAHjAgsxmDmhsBWXf7IkhgLp\n"+
            "SxZWNWgMRRQwhTzn6FaW+PvUB/E+Hv3tsw2Dd9brAoGAOt6yXZD00Oup9UNPwknT\n"+
            "dajidtMTlODqFYlGF/b0DhGw5Kq2AaTn8SwNPbrz1W8rGSjjZ+aKB+tSY4n8pQVZ\n"+
            "IlroY4U8Fws46Gsfvjsz1aKUPnC/u3P0HbW+Fz1j0ImVlQSsAyKCxgWeqVeul6Cy\n"+
            "JP25vcrPu0JXizCeQNsw0XsCgYEAmvXRceVG4WU2QpJdCZWqJJVenE60ZVsENT4D\n"+
            "sUg2Y2hRqaygetFqG29RPkIyQQ5xK2jttP2Y/Wv8+5123psyhqFAEZJ4Z381rVNc\n"+
            "TnO31CAdE7SJi9Tq5aIKSwjieGel9INWFR+Oi2rsKuwlClWua/cPJbKsFDStHGlv\n"+
            "S2Yc5A0CgYBtR0fjZjl5VeLeeyaqEW3lMK6fSdCFpfB1/5/4u6SvaaD2qfNaGcGT\n"+
            "xtTcOqnWw0jujCY2U83TdGQ8z+jvAyd1vcg644Sn7IGEbtUZdsKkPHjYChuQjPny\n"+
            "5b5PI2oMsF6XE/vUF7J3HMT5cDxqtcmCV4FZ7bkWVmrfpCKimFD0+A==\n"+
            "-----END RSA PRIVATE KEY-----";

    public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAumjXsEB4c7zIZFcM+A5Z\n" +
            "e7fv/+gvLx99EIKei5ueYecpQMliGIn0ae1S/LhX+hRHwCbpM2HrXlEVx52R1Hs/\n" +
            "Fcc2HPiWDw5KFiGKbVa8PXAYRBZRWiyT/tJq3uEgHmCJ0JjuIUtB8yl24slxn9rt\n" +
            "OPw3Jp56m9VEQJp7tYC8gs6CvTzAgiZTgVP0IgMuYVq5cIGAzAPBOeZZIT6rnNgK\n" +
            "KfZKZFGO82ToR1goVEI/bXtXIKR9WSMudURe1ecELy8LRHzR1BiogSkqOS2wYGM+\n" +
            "yTXrMOLIFRI3PN3WX/EzeTJnQ3YbJ7wve/19wOgJ4qhXZL9mNwpi0R/gFf7UERJ3\n" +
            "EQIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
